package com.wawa.bankservice.service;

import java.sql.Timestamp;
import java.util.Date;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wawa.bankservice.db.model.Account;
import com.wawa.bankservice.db.model.FundTransfer;
import com.wawa.bankservice.dto.AccountDetailsDTO;
import com.wawa.bankservice.dto.FundTransferReqDto;
import com.wawa.bankservice.dto.PaymentResDto;
import com.wawa.bankservice.dto.Status;
import com.wawa.bankservice.dto.WalletPaymentResponse;
import com.wawa.bankservice.dto.WalletTransferRequestDto;
import com.wawa.bankservice.repository.AccountRepository;
import com.wawa.bankservice.repository.FundTransferRepository;

@Service
@Transactional
public class AccountServiceImpl implements AccountService {

	@Autowired
	private FundTransferRepository fundTransferRepository;

	@Autowired
	private AccountRepository accountRepository;

	private static String E_KART_ACCOUNT_NUMBER = "87654321";

	@Override
	public PaymentResDto payment(FundTransferReqDto fundTransferReqDto) {

		PaymentResDto paymentResDto = null;
		try {
			FundTransfer fundTransfer = new FundTransfer();
			BeanUtils.copyProperties(fundTransferReqDto, fundTransfer);
			Date currentDate = new Date();
			fundTransfer.setTransactionDate(new Timestamp(currentDate.getTime()));
			fundTransfer.setToAccountNumber(E_KART_ACCOUNT_NUMBER);
			Account userAccount = accountRepository.findByAccountNumber(fundTransferReqDto.getFromAccountNumber());
			Account ekartAccount = accountRepository.findByAccountNumber(E_KART_ACCOUNT_NUMBER);
			if (userAccount != null && userAccount.getAmount() > fundTransferReqDto.getAmountTransferred()) {
				if (this.transaction(ekartAccount, userAccount, fundTransfer))
					paymentResDto = new PaymentResDto(fundTransfer.getAmountTransferred(), "SUCCESS");
			} else {
				return new PaymentResDto(0, "IN SUFFICIENT BALANCE");
			}

		} catch (Exception e) {
			System.err.println(e.getMessage());
		}

		return paymentResDto;
	}

	@Override
	public AccountDetailsDTO checkBalance(String accountNumber) {

		AccountDetailsDTO accountDetailsDTO = null;

		try {
			Account account = accountRepository.findByAccountNumber(accountNumber);
			if (account != null) {
				accountDetailsDTO = new AccountDetailsDTO();
				BeanUtils.copyProperties(account, accountDetailsDTO);
			}

		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		return accountDetailsDTO;
	}

	private boolean addAmount(Account account, double amount) throws Exception {
		try {
			account.setAmount(account.getAmount() + amount);
			accountRepository.save(account);
		} catch (Exception e) {

			throw new Exception("bank service error");
		}

		return true;
	}

	private boolean deductAmount(Account account, double amount) throws Exception {
		try {
			account.setAmount(account.getAmount() - amount);
			accountRepository.save(account);
		} catch (Exception e) {

			throw new Exception("bank service error");
		}

		return true;
	}

	private boolean transaction(Account toAccount, Account fromAccount, FundTransfer fundTransfer) {

		try {
			fundTransfer = fundTransferRepository.save(fundTransfer);
			this.deductAmount(fromAccount, fundTransfer.getAmountTransferred());
			this.addAmount(toAccount, fundTransfer.getAmountTransferred());

		} catch (Exception e) {
			return false;
		}
		return true;
	}

	@Override
	public String validate(String phoneNumber) {
		String status = null;
		try {
			if (validatePhoneNumber(phoneNumber) != null) {
				status = Status.VALID.toString();
			} else {
				status = Status.NOT_VALID.toString();
			}
		} catch (Exception e) {

			status = Status.NOT_VALID.toString();
		}
		return status;
	}

	private Account validatePhoneNumber(String phoneNumber) {
		return accountRepository.findByphoneNumber(phoneNumber) != null
				? accountRepository.findByphoneNumber(phoneNumber)
				: null;
	}

	@Override
	public WalletPaymentResponse walletPayment(WalletTransferRequestDto walletTransferRequestDto) {
		WalletPaymentResponse paymentResponse = null;
		Date currentDate = new Date();
		try {
			// need to validate phoneNumbers
			Account toAccount = validatePhoneNumber(walletTransferRequestDto.getToPhoneNumber());
			Account fromAccount = validatePhoneNumber(walletTransferRequestDto.getFromPhoneNumber());
			if (toAccount == null || fromAccount == null) {
				return new WalletPaymentResponse(null, Status.NOT_VALID, "Phone Numbers is not valid. please check");
			}

			FundTransfer fundTransfer = new FundTransfer();
			BeanUtils.copyProperties(walletTransferRequestDto, fundTransfer);
			fundTransfer.setTransactionDate(new Timestamp(currentDate.getTime()));
			fundTransfer.setFromAccountNumber(fromAccount.getAccountNumber());
			fundTransfer.setToAccountNumber(toAccount.getAccountNumber());

			if (fromAccount.getAmount() > walletTransferRequestDto.getAmountTransferred()) {
				if (this.transaction(toAccount, fromAccount, fundTransfer))
					paymentResponse = new WalletPaymentResponse(fundTransfer.getTransactionDate(), Status.SUCCESS,
							null);
			} else {
				paymentResponse = new WalletPaymentResponse(fundTransfer.getTransactionDate(), Status.FAIL,
						"Insufficient Balance");
			}
		} catch (Exception e) {
			paymentResponse = new WalletPaymentResponse(new Timestamp(currentDate.getTime()), Status.FAIL,
					e.getMessage());
		}
		return paymentResponse;
	}
}
