package com.wawa.bankservice.service;


import com.wawa.bankservice.dto.AccountDetailsDTO;
import com.wawa.bankservice.dto.FundTransferReqDto;
import com.wawa.bankservice.dto.PaymentResDto;
import com.wawa.bankservice.dto.WalletPaymentResponse;
import com.wawa.bankservice.dto.WalletTransferRequestDto;

public interface AccountService {

	PaymentResDto payment(FundTransferReqDto fundTransferReqDto);
	AccountDetailsDTO  checkBalance(String accountNumber);
	String  validate(String phoneNumber);
	WalletPaymentResponse walletPayment(WalletTransferRequestDto walletTransferRequestDto);
}
