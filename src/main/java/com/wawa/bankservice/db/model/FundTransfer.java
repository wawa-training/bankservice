// Generated with g9.

package com.wawa.bankservice.db.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "fund_transfer")
public class FundTransfer implements Serializable {

	/** Primary key. */
	protected static final String PK = "id";

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(unique = true, nullable = false, precision = 10)
	private int id;
	@Column(name = "amount_transferred", length = 22)
	private double amountTransferred;
	@Column(name = "to_account_number", length = 255)
	private String toAccountNumber;
	@Column(name = "from_account_number", length = 255)
	private String fromAccountNumber;
	@Column(name = "transaction_date")
	private Timestamp transactionDate;
	@Column(name = "transaction_message", length = 255)
	private String transactionMessage;

	/** Default constructor. */
	public FundTransfer() {
		super();
	}

	/**
	 * Access method for id.
	 *
	 * @return the current value of id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Setter method for id.
	 *
	 * @param aId the new value for id
	 */
	public void setId(int aId) {
		id = aId;
	}

	/**
	 * Access method for amountTransferred.
	 *
	 * @return the current value of amountTransferred
	 */
	public double getAmountTransferred() {
		return amountTransferred;
	}

	/**
	 * Setter method for amountTransferred.
	 *
	 * @param aAmountTransferred the new value for amountTransferred
	 */
	public void setAmountTransferred(double aAmountTransferred) {
		amountTransferred = aAmountTransferred;
	}

	/**
	 * Access method for toAccountNumber.
	 *
	 * @return the current value of toAccountNumber
	 */
	public String getToAccountNumber() {
		return toAccountNumber;
	}

	/**
	 * Setter method for toAccountNumber.
	 *
	 * @param aToAccountNumber the new value for toAccountNumber
	 */
	public void setToAccountNumber(String aToAccountNumber) {
		toAccountNumber = aToAccountNumber;
	}

	/**
	 * Access method for transactionDate.
	 *
	 * @return the current value of transactionDate
	 */
	public Timestamp getTransactionDate() {
		return transactionDate;
	}

	/**
	 * Setter method for transactionDate.
	 *
	 * @param aTransactionDate the new value for transactionDate
	 */
	public void setTransactionDate(Timestamp aTransactionDate) {
		transactionDate = aTransactionDate;
	}

	/**
	 * Access method for transactionMessage.
	 *
	 * @return the current value of transactionMessage
	 */
	public String getTransactionMessage() {
		return transactionMessage;
	}

	/**
	 * Setter method for transactionMessage.
	 *
	 * @param aTransactionMessage the new value for transactionMessage
	 */
	public void setTransactionMessage(String aTransactionMessage) {
		transactionMessage = aTransactionMessage;
	}

	public String getFromAccountNumber() {
		return fromAccountNumber;
	}

	public void setFromAccountNumber(String fromAccountNumber) {
		this.fromAccountNumber = fromAccountNumber;
	}

	/**
	 * Compares the key for this instance with another FundTransfer.
	 *
	 * @param other The object to compare to
	 * @return True if other object is instance of class FundTransfer and the key
	 *         objects are equal
	 */
	private boolean equalKeys(Object other) {
		if (this == other) {
			return true;
		}
		if (!(other instanceof FundTransfer)) {
			return false;
		}
		FundTransfer that = (FundTransfer) other;
		if (this.getId() != that.getId()) {
			return false;
		}
		return true;
	}

	/**
	 * Compares this instance with another FundTransfer.
	 *
	 * @param other The object to compare to
	 * @return True if the objects are the same
	 */
	@Override
	public boolean equals(Object other) {
		if (!(other instanceof FundTransfer))
			return false;
		return this.equalKeys(other) && ((FundTransfer) other).equalKeys(this);
	}

	/**
	 * Returns a hash code for this instance.
	 *
	 * @return Hash code
	 */
	@Override
	public int hashCode() {
		int i;
		int result = 17;
		i = getId();
		result = 37 * result + i;
		return result;
	}

	/**
	 * Returns a debug-friendly String representation of this instance.
	 *
	 * @return String representation of this instance
	 */
	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer("[FundTransfer |");
		sb.append(" id=").append(getId());
		sb.append("]");
		return sb.toString();
	}

	/**
	 * Return all elements of the primary key.
	 *
	 * @return Map of key names to values
	 */
	public Map<String, Object> getPrimaryKey() {
		Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
		ret.put("id", Integer.valueOf(getId()));
		return ret;
	}

}
