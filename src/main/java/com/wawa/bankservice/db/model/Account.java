// Generated with g9.

package com.wawa.bankservice.db.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

@Entity
@Table(name="account", indexes={@Index(name="account_account_number_IX", columnList="account_number", unique=true)})
public class Account implements Serializable {

    /** Primary key. */
    protected static final String PK = "accountId";

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    @Column(name="account_id", unique=true, nullable=false, precision=10)
    private int accountId;
    @Column(name="account_number", unique=true, nullable=false, length=45)
    private String accountNumber;
    @Column(name="account_name", nullable=false, length=255)
    private String accountName;
    @Column(nullable=false, precision=22)
    private double amount;
    @Column(name="is_active")
    private boolean isActive;
    @Column(name="creation_date")
    private Timestamp creationDate;
    @Column(name="modified_date")
    private Timestamp modifiedDate;
    
    @Column(name="phone_number", unique=true, nullable=false, length=45)
    private String phoneNumber;

    /** Default constructor. */
    public Account() {
        super();
    }

    /**
     * Access method for accountId.
     *
     * @return the current value of accountId
     */
    public int getAccountId() {
        return accountId;
    }

    /**
     * Setter method for accountId.
     *
     * @param aAccountId the new value for accountId
     */
    public void setAccountId(int aAccountId) {
        accountId = aAccountId;
    }

    /**
     * Access method for accountNumber.
     *
     * @return the current value of accountNumber
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * Setter method for accountNumber.
     *
     * @param aAccountNumber the new value for accountNumber
     */
    public void setAccountNumber(String aAccountNumber) {
        accountNumber = aAccountNumber;
    }

    /**
     * Access method for accountName.
     *
     * @return the current value of accountName
     */
    public String getAccountName() {
        return accountName;
    }

    /**
     * Setter method for accountName.
     *
     * @param aAccountName the new value for accountName
     */
    public void setAccountName(String aAccountName) {
        accountName = aAccountName;
    }

    /**
     * Access method for amount.
     *
     * @return the current value of amount
     */
    public double getAmount() {
        return amount;
    }

    /**
     * Setter method for amount.
     *
     * @param aAmount the new value for amount
     */
    public void setAmount(double aAmount) {
        amount = aAmount;
    }

    /**
     * Access method for isActive.
     *
     * @return true if and only if isActive is currently true
     */
    public boolean getIsActive() {
        return isActive;
    }

    /**
     * Setter method for isActive.
     *
     * @param aIsActive the new value for isActive
     */
    public void setIsActive(boolean aIsActive) {
        isActive = aIsActive;
    }

    /**
     * Access method for creationDate.
     *
     * @return the current value of creationDate
     */
    public Timestamp getCreationDate() {
        return creationDate;
    }

    /**
     * Setter method for creationDate.
     *
     * @param aCreationDate the new value for creationDate
     */
    public void setCreationDate(Timestamp aCreationDate) {
        creationDate = aCreationDate;
    }

    /**
     * Access method for modifiedDate.
     *
     * @return the current value of modifiedDate
     */
    public Timestamp getModifiedDate() {
        return modifiedDate;
    }

    /**
     * Setter method for modifiedDate.
     *
     * @param aModifiedDate the new value for modifiedDate
     */
    public void setModifiedDate(Timestamp aModifiedDate) {
        modifiedDate = aModifiedDate;
    }

    
    
    public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	/**
     * Compares the key for this instance with another Account.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Account and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Account)) {
            return false;
        }
        Account that = (Account) other;
        if (this.getAccountId() != that.getAccountId()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Account.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Account)) return false;
        return this.equalKeys(other) && ((Account)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getAccountId();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Account |");
        sb.append(" accountId=").append(getAccountId());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("accountId", Integer.valueOf(getAccountId()));
        return ret;
    }

}
