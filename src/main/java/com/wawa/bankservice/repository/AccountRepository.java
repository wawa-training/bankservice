package com.wawa.bankservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.wawa.bankservice.db.model.Account;

public interface AccountRepository extends JpaRepository<Account, Integer> {

	Account findByAccountNumber(String accountNumber);
	Account findByphoneNumber(String phoneNumber);
}
