package com.wawa.bankservice.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wawa.bankservice.db.model.FundTransfer;


@Repository
public interface FundTransferRepository extends JpaRepository<FundTransfer, Integer> {

}
