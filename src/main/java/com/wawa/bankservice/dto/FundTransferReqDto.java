package com.wawa.bankservice.dto;

import java.sql.Timestamp;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;


@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FundTransferReqDto {

	
	
	    public FundTransferReqDto(double amountTransferred, String fromAccountNumber, 
			Timestamp transactionDate, String transactionMessage) {
		super();
		this.amountTransferred = amountTransferred;
		this.fromAccountNumber = fromAccountNumber;
		this.transactionDate = transactionDate;
		this.transactionMessage = transactionMessage;
	}
	    
	    
		public FundTransferReqDto() {
			super();
			// TODO Auto-generated constructor stub
		}


		private double amountTransferred;
	    private String fromAccountNumber;
	    private Timestamp transactionDate;
	    private String transactionMessage;
		public double getAmountTransferred() {
			return amountTransferred;
		}


		public void setAmountTransferred(double amountTransferred) {
			this.amountTransferred = amountTransferred;
		}


		public String getFromAccountNumber() {
			return fromAccountNumber;
		}


		public void setFromAccountNumber(String fromAccountNumber) {
			this.fromAccountNumber = fromAccountNumber;
		}


		


		public Timestamp getTransactionDate() {
			return transactionDate;
		}


		public void setTransactionDate(Timestamp transactionDate) {
			this.transactionDate = transactionDate;
		}


		public String getTransactionMessage() {
			return transactionMessage;
		}


		public void setTransactionMessage(String transactionMessage) {
			this.transactionMessage = transactionMessage;
		}


		@Override
		public String toString() {
			return "FundTransferDto [amountTransferred=" + amountTransferred + ", fromAccountNumber="
					+ fromAccountNumber + ", transactionDate="
					+ transactionDate + ", transactionMessage=" + transactionMessage + "]";
		}
	    
	    
	    
	
	
}
