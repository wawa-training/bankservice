package com.wawa.bankservice.dto;


public class AccountDetailsDTO {

	
    public AccountDetailsDTO() {
		super();
		// TODO Auto-generated constructor stub
	}
	private String accountName;
    private double amount;
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
    
    
}
