package com.wawa.bankservice.dto;

import java.sql.Timestamp;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WalletTransferRequestDto {

	public WalletTransferRequestDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public WalletTransferRequestDto(double amountTransferred, String fromPhoneNumber, String toPhoneNumber,
			Timestamp transactionDate, String transactionMessage) {
		super();
		this.amountTransferred = amountTransferred;
		this.fromPhoneNumber = fromPhoneNumber;
		this.toPhoneNumber = toPhoneNumber;
		this.transactionDate = transactionDate;
		this.transactionMessage = transactionMessage;
	}

	@NotNull
	private double amountTransferred;
	@NotEmpty(message="Phone Number should not empty")
	private String fromPhoneNumber;
	@NotEmpty(message="Phone Number should not empty")
	private String toPhoneNumber;
	private Timestamp transactionDate;
	private String transactionMessage;

	public double getAmountTransferred() {
		return amountTransferred;
	}

	public void setAmountTransferred(double amountTransferred) {
		this.amountTransferred = amountTransferred;
	}

	public String getFromPhoneNumber() {
		return fromPhoneNumber;
	}

	public void setFromPhoneNumber(String fromPhoneNumber) {
		this.fromPhoneNumber = fromPhoneNumber;
	}

	public String getToPhoneNumber() {
		return toPhoneNumber;
	}

	public void setToPhoneNumber(String toPhoneNumber) {
		this.toPhoneNumber = toPhoneNumber;
	}

	public Timestamp getTransactionDate() {
		return transactionDate;
	}

	public void setTransactionDate(Timestamp transactionDate) {
		this.transactionDate = transactionDate;
	}

	public String getTransactionMessage() {
		return transactionMessage;
	}

	public void setTransactionMessage(String transactionMessage) {
		this.transactionMessage = transactionMessage;
	}

	@Override
	public String toString() {
		return "WalletTransferRequestDto [amountTransferred=" + amountTransferred + ", fromPhoneNumber="
				+ fromPhoneNumber + ", toPhoneNumber=" + toPhoneNumber + ", transactionDate=" + transactionDate
				+ ", transactionMessage=" + transactionMessage + "]";
	}

}
