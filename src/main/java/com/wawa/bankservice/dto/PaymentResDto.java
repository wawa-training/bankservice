package com.wawa.bankservice.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PaymentResDto {

	
	
	public PaymentResDto() {
		super();
	}
	public PaymentResDto(double amountTransferred, String status) {
		super();
		this.amountTransferred = amountTransferred;
		this.status = status;
	}
	private double amountTransferred;
	private String status;
	public double getAmountTransferred() {
		return amountTransferred;
	}
	public void setAmountTransferred(double amountTransferred) {
		this.amountTransferred = amountTransferred;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
