package com.wawa.bankservice.dto;

import java.sql.Timestamp;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class WalletPaymentResponse {
	
	

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss")
    private Timestamp paymentDate;
	
	private Status status;
	private String message ;
	public WalletPaymentResponse(Timestamp paymentDate, Status status, String message) {
		super();
		this.paymentDate = paymentDate;
		this.status = status;
		this.message = message;
	}
	public WalletPaymentResponse() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Timestamp getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(Timestamp paymentDate) {
		this.paymentDate = paymentDate;
	}
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	@Override
	public String toString() {
		return "WalletPaymentResponse [paymentDate=" + paymentDate + ", status=" + status + ", message=" + message
				+ "]";
	}
	
	
	
	
}
