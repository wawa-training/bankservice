package com.wawa.bankservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.wawa.bankservice.dto.WalletPaymentResponse;
import com.wawa.bankservice.dto.WalletTransferRequestDto;
import com.wawa.bankservice.service.AccountService;

@RestController
@RequestMapping("/wallet/account")
public class WalletController {

	@Autowired
	private AccountService accountService;

	@GetMapping("/{phoneNumber}")
	public String validate(@PathVariable String phoneNumber) {
		return accountService.validate(phoneNumber);

	}

	@PostMapping("/payment")
	public WalletPaymentResponse walletPayment(@RequestBody WalletTransferRequestDto walletTransferRequestDto) {
		return accountService.walletPayment(walletTransferRequestDto);

	}

}
