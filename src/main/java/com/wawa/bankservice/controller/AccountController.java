package com.wawa.bankservice.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wawa.bankservice.dto.AccountDetailsDTO;
import com.wawa.bankservice.dto.FundTransferReqDto;
import com.wawa.bankservice.dto.PaymentResDto;
import com.wawa.bankservice.service.AccountService;

@RestController
@RequestMapping("/account")
public class AccountController {

	@Autowired
	private AccountService accountService;

	@PostMapping("/payment")
	public PaymentResDto payment(@RequestBody FundTransferReqDto fundTransferDto) {

		return accountService.payment(fundTransferDto);

	}

	@GetMapping("/balance")
	public AccountDetailsDTO checkBalance(@RequestParam(required = true) String accountNumber) {

		return accountService.checkBalance(accountNumber);
	}
}
